Ongoing, robust network measurement is essential
in order to respond to censorship events, to adapt Tor Browser and other apps to
respond to changing network conditions, and to validate changes to the
Tor network software.

Section~\ref{sec:public-datasets} describes the information that the Tor Metrics
team collects about Tor users and the Tor network;
Section~\ref{censorship-of-tor} describes information that the Open Observatory
of Network Interference (OONI)~\cite{ooni-paper} and the Tor Networks team collect about censorship of the Tor network. 

\subsection{Tor Metrics public datasets}
\label{sec:public-datasets}

Tor Metrics \cite{wecsr10measuring-tor} archives historical data about the Tor
ecosystem, collects data from the public Tor network and related services, and
helps develop novel approaches to safe, privacy preserving data collection.

Tor relays and bridges collect aggregated statistics about the bandwidth they
use and the numbers of connecting clients per country. The relays protect user
privacy by discarding IP addresses and only reporting country information from
a local database that maps IP address ranges to countries. The relays aggregate
these statistics and send them to the directory authorities periodically.

CollecTor~\cite{collector}
archives the latest server descriptors, ``extra info'' descriptors containing the
aggregated statistics, and consensus documents from the directory authorities.
This archive is public, and researchers can use the metrics-lib Java
library~\cite{metrics-lib}
to perform analysis of the data.

In order to provide easy access to visualizations of the archived historical
data, the Tor Metrics website~\cite{tor-metrics}
contains a number of customizable plots to show user, traffic, relay, bridge,
and application download statistics over a requested time period for a
particular country.

\subsubsection{Relay and bridge user counts}

The number of Tor users is one of our most important statistics. It is vital
to know how many people use the Tor network on a daily basis, whether
they connect via relays or bridges, from which countries they connect, what
protocols (transports) they use, and whether they connect via IPv4 or IPv6.

Because Tor is an anonymity network, we cannot collect
identifying data to learn the number of users. So we don't actually
count users, but rather requests to the directories or bridges that clients
make to update their list of relays in the network. We estimate user
numbers from this information.

The result is an average number of concurrent users, estimated from data
collected over a day, but not how many distinct users there are. It is not
possible to know whether the same set of users stays connected over the whole
day, or if that set leaves after a few hours and a new set of users arrives.
However, our main interest is finding out if usage changes, so it is not
critical to estimate absolute user numbers.

Relay users are users that connect directly to a relay in order to connect to
the Tor network; bridge users connect to a bridge as an entry point into the
Tor network.

\subsubsection{Safe counting and PrivCount}

Recent advances in privacy-preserving metrics collection such as private
set-union cardinality~\cite{psc-ccs2017} and PrivCount~\cite{privcount-ccs2016}
can help to improve our user metrics.

Private set-union cardinality is useful in many settings. It can determine how
many unique users are using Tor, how many unique users connect via a particular
version of client software, and how many unique destinations are contacted. It
could also be used to determine how users regularly connect to the network
(\eg over mobile connections, through proxies, etc.) and how the network is
used (\eg the length of time that users spend on the network in a single
session).

PrivCount implements a privacy-preserving scheme for distributed measurement
using secure multiparty computation. It provides a method of collecting
network-wide aggregate statistics without revealing the totals provided by each
individual relay. By adding carefully calculated noise, this scheme can also
provide differential privacy guarantees.

Very recent work~\cite{tor-usage-priv-measurement-imc18} has enhanced PrivCount
and Private set-union cardinality and used them to measure numbers of Tor
users. The results show a much higher number of users than we have
previously estimated: closer to eight million daily users rather than the
early count of two million daily users.
We need to closely examine the methodologies used in this work to validate its
findings.

\subsubsection{Reliable geolocation}

In producing metrics relating to countries, for example the top countries
by daily users shown in Table~\ref{tbl:top-user-countries}, Tor Metrics
uses IP geolocation databases. We currently use the MaxMind GeoLite2 City
database~\cite{max-mind-geolite2}, but a strong reliance on a single source of this data is undesirable.
For calculating metrics relating to relays---where IP addresses are
already public---we also look up autonomous system (AS) information in
these databases to better understand the diversity of the network (as discussed
further in Section~\ref{sec:network-health}).

\begin{table}
\begin{center}
\begin{tabular}{ | l | l | }
\hline
\textbf{Country} & \textbf{Mean daily users} \\
\hline
\hline
United States & 367814 (17.70 \%) \\
\hline
United Arab Emirates & 257893 (12.41 \%) \\
\hline
Russia & 244418 (11.76 \%) \\
\hline
Germany & 153335 (7.38 \%) \\
\hline
France & 105256 (5.06 \%) \\
\hline
Indonesia & 95731 (4.61 \%) \\
\hline
Ukraine & 77508 (3.73 \%) \\
\hline
United Kingdom & 62134 (2.99 \%) \\
\hline
India & 45891 (2.21 \%) \\
\hline
Netherlands & 42741 (2.06 \%) \\
\hline
\end{tabular}
\end{center}
\caption{The top-10 countries by estimated number of directly-connecting
clients between the 17th July and the 15th October 2018. These numbers are
derived from directory requests counted on directory authorities and mirrors.
Relays resolve client IP addresses to country codes locally using an
IP geolocation database.}
\label{tbl:top-user-countries} \end{table}

One alternative for country code lookups is to use a database from
IP2Location~\cite{ip2location}. We have not yet
explored the benefits of making this change and would be unlikely to be able to
distribute this database as we currently do with the MaxMind database.
% ^ why not? XXX

One alternative for AS number resolution is RIPEstat~\cite{ripe-stat} which
uses BGP feeds to provide near-live information. In a comparison study by Tor
Metrics on the 1st July 2018~\cite{tor-metrics-geo-study}, there were 269
relays where there was disagreement between RIPEstat and the latest MaxMind
database and 7889 in agreement ($\kappa = 0.979$ excluding relays for which
either MaxMind or RIPEstat had no AS number). There were 101 relays for which
MaxMind did not return an AS number and 2 relays for which RIPEstat did not
return an AS number.

The MaxMind database is distributed to users as part of the Tor software, and
it can be used for example to
disable or prefer the use of exit relays in specific countries. So it may be
dangerous for users if they get mixed information about the autonomous system
numbers or country codes assigned to relays. It may be equally dangerous to
incorrectly assign country codes, but without ground truth to compare to, it is
not possible to say whether a switch would improve this situation or not.

We hope to analyze the different databases and feeds available
to us to determine which options best fit our requirements.

\subsubsection{Network performance}
\label{sec:onionperf}

The performance that Tor users experience depends on many factors and is
the subject of current research. In order to evaluate progress in improving
Tor's performance, we need to continuously measure how fast Tor really is for
our users.

In June 2009 we began longitudinal measurements of Tor performance using the
Torperf~\cite{torperf} tool. It used a
Tor client with the same path selection algorithms as an ordinary user would
use and performed downloads of a 50~KB file, a 1~MB file, and a 5~MB file. The
initial findings from these measurements~\cite{tor-2009-09-001} and the
ongoing measurements, visualized in Figure~\ref{fig:torperf}, provided a
feedback loop for developers and those looking after the network to understand
the effects that changes could have for the key performance characteristics:
throughput and latency.

\begin{figure}
\begin{center}
\includegraphics[width=0.8\textwidth]{task11b/torperf-public-2009-06-10-2017-05-01-torperf-5mb.png}
\end{center}
\caption{Overall performance when downloading a 5 MB static file over Tor from
a server on the public internet between July 2009 and April 2017. The graph
shows the range of measurements from first to third quartile, and highlights
the median. The slowest and fastest quarter of measurements are omitted from
the graph.}
\label{fig:torperf}
\end{figure}

OnionPerf~\cite{onionperf} is a modern
rewrite of Torperf that includes more fine-grained detail in the measurements.
OnionPerf has the ability to measure the performance of onion services from a
user perspective in addition to the traditional measurement using a server on
the public Internet. In April 2017, Tor Metrics began using OnionPerf to
perform measurements of the Tor network, replacing Torperf. We currently
convert OnionPerf's output to a format compatible with Torperf, so Tor
Metrics does not yet handle all the new data that is captured.

We have, however, introduced new latency visualizations to the Tor
Metrics website using OnionPerf data. For web browsing and other
interactive applications, latency can be as important as throughput in
improving the quality of end-user experience.
Figure~\ref{fig:onionperf-latencies} shows the average latencies measured
for circuits
since Tor Metrics began running OnionPerf.

\begin{figure}
\begin{center}
\includegraphics[width=0.8\textwidth]{task11b/onionperf-latencies-2017-04-15-2018-10-12-all.png}
\end{center}
\caption{Round-trip latencies of circuits used for downloading static files of
different sizes over Tor, either from a server on the public internet or from a
version 2 onion server. Round-trip latencies are measured as the time between
sending the HTTP request and receiving the HTTP response header. The graph
shows the range of measurements from first to third quartile, and highlights
the median. The slowest and fastest quarter of measurements are omitted from
the graph.}
\label{fig:onionperf-latencies}
\end{figure}

The Tor Metrics OnionPerf measurements are currently performed using vantage
points in 3 data centers of a single network operator. In the future it is
important that these vantage points are diversified to enable a view of the
network that is closer to that of a real user. More realistic vantage
points could include residential (DSL or DOCSIS) or cellular (3G or LTE)
access networks.
Further, to understand the performance for users in denied countries,
performance measurements should be carried out using pluggable transports.

\subsection{Detecting censorship of Tor}
\label{censorship-of-tor}

The Tor network offers online anonymity, privacy, and censorship circumvention.
Human rights defenders rely on the Tor network to secure their communications,
circumvent the blocking of social media apps during protests, and to securely
report on corruption and other abuses of power. As a result, the Tor network
has become a target of censorship by several governments around the world. In
such countries, users can circumvent the blocking and connect to the Tor
network through the use of bridges. In some cases though, bridges are
blocked too.

The Open Observatory of Network Interference (OONI)~\cite{ooni-paper} performs
active measurement to detect censorship. Through the Tor Metrics described in
the previous section, we track the expanding usage of Tor and Tor bridges
globally. These metrics allow us to infer when access to the Tor network is
blocked, particularly when we see spikes in Tor bridge usage. In addition to
our measurement projects, we also receive alerts on Tor blocking from our
community members.

In most countries around the world, the Tor network appears to be accessible.
In recent years, however, we have (mainly) observed the blocking of the Tor
network in the following countries: Egypt, Ethiopia, China, Iran, Turkey,
and Venezuela. 

\subsubsection{Open Observatory of Network Interference}
\label{ooni}

Over the last several years, we have been monitoring the blocking of the Tor
network through our censorship measurement software, OONI
Probe~\cite{ooni-probe}, which includes
tests~\cite{ooni-nettest} designed to measure
the reachability of the Tor network and of Tor bridges from the vantage point
of the user. Every month, hundreds of thousands of measurements are collected
and published~\cite{ooni-explorer} from
thousands of networks in more than 200
countries~\cite{ooni-stats}. These measurements enable
us to track the blocking of the Tor network around the world. 

The ``vanilla Tor" test built in to OONI Probe is the primary means of
detecting censorship of the Tor network. The vanilla Tor test attempts to make
a connection to the Tor network. If the test successfully establishes a
connection within a predefined number of seconds (300 by default), then Tor is
considered to be reachable from the vantage point of the user. But if the test
does not manage to establish a connection, then the Tor network is likely
blocked within the tested network. This test was central to the analyses in the
upcoming sections.

There is also a ``Tor Bridge Reachability" test that examines whether Tor
bridges work in tested networks. This test runs Tor with a list of bridges and
if it's able to connect to them successfully, we conclude that Tor bridges are
not blocked in the tested network. If the test, however, is unable to make
a connection, then the Tor bridges are either offline or blocked.

OONI Probe requires volunteers to run measurements and those volunteers must be
located in the target countries. To engage volunteers, OONI has established a
Partnership
Program~\cite{ooni-partner-prog}
to collaborate with local digital rights organizations on censorship
measurement research. Over the last several years, OONI has established 25
partnerships, many of which involve digital rights organizations in countries
where Tor is blocked, such as Egypt and Iran. We will continue to expand our
community engagement activities to monitor the blocking of Tor and to make it
more resilient to censorship. 

\paragraph{Egypt}

Following the blocking of hundreds of media
websites~\cite{ooni-egypt-summary},
over the last year, more and more Egyptians began to use censorship circumvention technologies. Egyptian ISPs then started blocking numerous circumvention
tools, including the Tor network.

Last July, OONI
published a research
report~\cite{ooni-egypt-report}
documenting Tor blocking. This study includes an analysis of all network
measurements~\cite{ooni-explorer-egypt} collected from Egypt between January 2017 to May 2018. These
measurements suggest that access to the Tor network was blocked, since the
tests weren't able to make connections to the Tor network within 300
seconds. More than 460 measurements showed that connections to the Tor network
consistently failed, strongly suggesting that access to it was blocked. 

Tor bridges appear to be blocked by some ISPs in Egypt as well. Vodafone, in
particular, appears to be blocking the default \textit{obfs4} bridges that
are shipped as part of Tor
Browser. (Anecdotally, we hear reports that private obfs4 bridges still work,
but we don't have OONI test results to confirm this conclusion.)
All measurements collected from
state-owned Telecom Egypt, on the other hand, show that the built-in
\textit{obfs4} bridges work.

This is not the first time that we have observed Tor censorship in Egypt. In
2016, when Egyptian ISPs started to block media sites, we found that the Tor
connection process was being disrupted via the blocking of requests to directory
authorities. 

Figure~\ref{fig:vanilla-tor-eg} illustrates the total number of tests
that successfully connected to the Tor network versus those that failed between
June 2017 and November 2017 from seven distinct autonomous system numbers
(ASNs) in Egypt. 211 tests failed and only 38 test succeeded in
connecting to the Tor network.

\begin{figure}
\begin{center}
\includegraphics[width=0.5\textwidth]{task11a/eg-success-failure.png}
\end{center}
\caption{Success and failure rates for vanilla Tor measurements performed by
OONI Probe in Egypt between June and November 2017.}
\label{fig:vanilla-tor-eg}
\end{figure}

Recent OONI measurements suggest that access to the Tor network, when
not using a private obfs4 bridge, remains blocked in Egypt.

\paragraph{Ethiopia}

During a wave of protests in
2016~\cite{bbc-ethiopia-protests}, numerous
Ethiopian media outlets and political opposition websites
were blocked.
WhatsApp was censored as well, along with several circumvention tool sites,
including the Tor website. 

In collaboration with Amnesty International, we published a research
report~\cite{ooni-ethiopia}
documenting these censorship events based on the analysis of OONI network
measurements collected from Ethiopia. While we found that access to our site
(torproject.org) was blocked, we did not find the Tor software itself being
blocked in Ethiopia during the testing period. To enable Tor usage (despite our
site being blocked), we shared information on how to download Tor Browser from
an alternative site~\cite{gettor}.

Over the last year, however, access to the Tor network has been blocked as
well. Most OONI measurements collected from Ethiopia~\cite{ooni-explorer-ethiopia} in 2017 show that attempts
to establish connections to the Tor network were unsuccessful, strongly
suggesting that access is blocked. 

Under a new Prime
Minister~\cite{ethiopia-new-pm}
who is from the Oromia region\textemdash which has been
protesting~\cite{bbc-ethiopia-protests}
against marginalization and persecution by authorities over the last few years\textemdash 
many reforms have taken place in Ethiopia over the past several months. These
include the unblocking of hundreds of websites, which OONI
reported\cite{ooni-ethiopia-unblock}
on in June. 

Access to the Tor Project website, however, remains blocked. Similarly, the
most recent measurements suggest that access to the Tor network remains blocked
as well. Bridge reachability tests have never been run in Ethiopia, and so it
remains unclear if access to Tor bridges is blocked.

\paragraph{China}

China is known for its pervasive internet censorship apparatus, which also
involves the blocking of numerous circumvention tools. Most OONI
measurements~\cite{ooni-explorer-china}
collected from local vantage points strongly suggest that access to the Tor
network is blocked, since most tests fail to establish connections. Between
June 2017 to November 2017, most measurements were unsuccessful, as seen
in Figure~\ref{fig:vanilla-tor-cn}.

\begin{figure}
\begin{center}
\includegraphics[width=0.5\textwidth]{task11a/cn-success-failure.png}
\end{center}
\caption{Success and failure rates for vanilla Tor measurements performed by
OONI Probe in China between June and November 2017.}
\label{fig:vanilla-tor-cn}
\end{figure}

In recent months, Tor reachability has been tested across multiple networks in
China. Table~\ref{tbl:vanilla-tor-cn} summarizes the findings from recent
measurements by autonomous system.

\begin{table}
\begin{tabular}{ | l | l | }
\hline
\textbf{Autonomous System} & \textbf{Vanilla Tor measurement result} \\
\hline
\hline
Shanghai Mobile Communications (AS 24400) & Failure \\
\hline
China Telecom (AS 4812) & Success \\
\hline
Guangdong Mobile Communications (AS 9808) & Failure \\
\hline
China Education \& Research Network Center (AS 4538) & Success \\
\hline
China Unicom (AS 4808) & Failure \\
\hline
China Telecom Backbone (AS 4134) & Failure \\
\hline
China Mobile Communications (AS 56040) & Failure \\
\hline
\end{tabular}
\caption{Recent OONI Probe vanilla Tor measurement results by autonomous system
for Chinese networks}
\label{tbl:vanilla-tor-cn}
\end{table}

Access to the Tor network appears to be blocked in most networks, since almost
all measurements from those ASNs consistently fail to
establish connections. It's worth noting, though, that the Tor network appears
to be accessible from some Chinese networks, such as China Telecom (AS 4812) and
the China Education \& Research Network Center (AS 4538). Bridge reachability
tests haven't been run in China in recent years, limiting our ability to
evaluate whether they work locally or not. 

\paragraph{Iran}

The breadth and depth of internet censorship in Iran is pervasive. Thousands of
OONI Probe
measurements~\cite{ooni-explorer-iran}
collected from 60 local networks in Iran over the last several years confirm the
blocking~\cite{ooni-iran-report}
of more than 800 domains. These include media outlets, opposition sites,
pro-democracy sites, human rights sites, the blogs of Iranian political
activists, as well as numerous circumvention tool sites, including
torproject.org.

We also found the Tor network to be blocked in many networks in Iran, but 
accessible in some. While the domain of Tor's bridge database site  was blocked
(being a sub-domain of torproject.org), we found that some Tor bridges work in
some networks in Iran.

Recent OONI measurements suggest that Tor remains blocked in some networks,
like IranCell (AS 44244), but is accessible in other networks, like Aria Shatel
(AS 31549). In general, internet censorship in Iran does not appear to be
deterministic, as we've observed the dynamic blocking and unblocking of
services over time. Censorship measurement is therefore required on an ongoing
basis.

\paragraph{Turkey}

Direct connections to
the Tor network appear to be blocked in Turkey as well. Between June 2017 and
November 2017, most tests attempting to establish connections to the Tor
network failed, as can be seen in Figure~\ref{fig:vanilla-tor-tr}, based on
measurements~\cite{ooni-explorer-turkey}
collected from six distinct ASNs in Turkey. 

\begin{figure}
\begin{center}
\includegraphics[width=0.5\textwidth]{task11a/tr-success-failure.png}
\end{center}
\caption{Success and failure rates for vanilla Tor measurements performed by
OONI Probe in Turkey between June and November 2017.}
\label{fig:vanilla-tor-tr}
\end{figure}

Recent measurements collected from TurkNet (AS 12735) show that the Tor network
is accessible, while measurements collected from Tellcom Iletisim (AS 34984)
indicate that it's blocked, since most tests failed to establish connections to
the Tor network. This ISP also appears to be blocking access to Tor's website, but the site is accessible from other networks, such as
Vodafone (AS 15897). Bridge reachability tests have never been run by volunteers
in Turkey, limiting our ability to assess whether Tor bridges work locally.

\paragraph{Venezuela}

A few months ago, one ISP in
Venezuela blocked access to the Tor network as well.
Following months of increased censorship, particularly targeting a number of
local media websites and currency exchange sites, state-owned CANTV (AS 8048)
blocked access to the Tor network, possibly to prevent people from getting around censorship~\cite{ooni-venezuela-report}.

All OONI measurements~\cite{ooni-explorer-venezuela} collected from Venezuela up until 6th June 2018 were
successful, showing that the Tor network was accessible then. On 20th
June 2018, however, Tor testing on CANTV started to fail. Most other
measurements collected from 20th June 2018 onwards (collected from the same
network on an almost daily basis) failed as well, strongly suggesting that
CANTV blocked access to the Tor network. According to OONI's scans in
mid-August 2018 from CANTV, connections to 74\% of well-known IP address and
port combinations of the Tor network were blocked on the reverse path. 

A large number of obfs4 bridges were blocked as well. Bridge reachability tests
run from CANTV in late June 2018 show a failure rate of around 94\% to known Tor
bridges. Not all of these failures are necessarily caused by blocking, as some
bridges might be offline or unreachable. The high percentage of connection
failures, though, strongly suggests that well-known bridges are being blocked.
Repeated testing in August 2018 also presented a high percentage: 88\% of
running bridges were unreachable from a CANTV vantage point. It's worth noting,
though, that private bridges seemed to work. All testing to private Tor
bridges resulted in successful connections, regardless of the type of bridge.
This includes obfs4 bridges and bridges that don't use pluggable
transports---so we conclude that the blocking was based on IP address, and
not some more sophisticated DPI-based approach.

Over the last few months, CANTV has unblocked access to the Tor network. Further
testing on 2nd October 2018 revealed that around 97\% of public Tor nodes were
reachable from the vantage point of CANTV. While the precise
date of unblocking is unclear, Tor Metrics data seen in
Figure~\ref{fig:relayusers-ve} suggest that Tor may have been unblocked
on 30th August
2018, since we observe a spike in Tor usage on that day.

\begin{figure}
\begin{center}
\includegraphics[width=0.8\textwidth]{task11b/userstats-relay-country-ve-2018-07-14-2018-10-10-on.png}
\end{center}
\caption{Number of directly connecting Tor users from Venezuela between 1st May
and 10th October 2018. The darker grey range indicates the expected user
counts.  Blue and red spots indicate data points that fell either above or
below the expected range.}
\label{fig:relayusers-ve}
\end{figure}

The Tor Project website has remained accessible in CANTV (and other
networks) all along, even during the time that direct Tor network connections
and default obfs4 bridges were blocked.

\subsubsection{Tor Metrics anomaly detection}

Tor Metrics has an ``early warning system"~\cite{tor-2011-09-001} to indicate
anomalies in the counts of directly connecting Tor users. The detector is based
on a simple model of the number of users per day, per country. That model is
used to assess whether the number of users we observe is typical, too high, or
too low. In a nutshell, the prediction on any day is based on activity of
previous days, locally as well as worldwide.

This system is integrated into the Tor Metrics website and can annotate
visualizations with any abnormalities that are detected. When a country
puts new censorship in place this can appear in our user data as an increase in
directly connecting users in that country. As an example, Uganda introduced a
``social media
tax"~\cite{bbc-uganda-social-media-tax} on July
1st 2018. This can be seen in Figure~\ref{fig:relayusers-ug} for that date
as an unexpected rise in users.

Similarly, when a country introduces censorship of the Tor network this can
appear in our user data as a drop in directly connecting users. Many
networks in Venezuela blocked access to the Tor network between June and
August 2018~\cite{venezuela-blocks-tor}
and the effects of this can be seen in Figure~\ref{fig:relayusers-ve}.

\begin{figure}
\begin{center}
\includegraphics[width=0.8\textwidth]{task11b/userstats-relay-country-ug-2018-05-01-2018-08-01-on.png}
\end{center}
\caption{Number of directly connecting Tor users from Uganda between 1st May
and 1st August 2018. The darker grey range indicates the expected user counts.
Blue and red spots indicate data points that fell either above or below the
expected range.}
\label{fig:relayusers-ug}
\end{figure}

Another system~\cite{WrightDF15} developed and maintained by academic researchers, detects ongoing per-country
anomalies in the user counts. It identifies contiguous anomalous periods,
rather than daily spikes or drops, and allows anomalies to be ranked according
to deviation from expected behavior. This system is currently run continuously
and reports daily to a mailing
list~\cite{infolabe}
but is not yet integrated with Tor Metrics.

\subsubsection{Events timeline}

Tor Metrics maintains a
timeline~\cite{tor-metrics-news} of events
that might affect user metrics, to aid in the interpretation of data and
visualizations. This timeline is\textemdash by necessity\textemdash manually
maintained. Where possible each entry in the timeline is accompanied by links
to relevant visualizations, discussions, and news articles. We may create entries in response to reports from users or news outlets, automated anomaly
detection, analysis of data collected by OONI and many other sources.

Some example entries are shown in Table~\ref{tbl:timeline-examples}. When
viewing visualizations on the Tor Metrics website, relevant entries will be
shown under the graph for some visualizations.

\begin{table}
\begin{center}
\begin{tabular}{ | l | p{0.5\textwidth} | p{0.2\textwidth} | }
\hline
\textbf{Date} & \textbf{Entry} & \textbf{Links} \\
\hline
\hline
2018-07-01 to present & A social media tax takes effect in Uganda. The
government pressures ISPs to block VPNs. & User graphs, BBC article, AllAfrica
article \\
\hline
2018-07-28 to present & Another jump of relay users in Turkey, from 5k to about
30k. & User graphs \\
\hline
2018-07-29 to 2018-08-08 & Protests over road safety in Dhaka, Bangladesh.
Reports of mobile network throttling and blocking of Facebook. & New York Times
article, Daily Star article \\
\hline
\end{tabular}
\end{center}
\caption{Selected entries from the Tor Metrics Timeline that can provide
context when interpreting the user count data and visualizations.}
\label{tbl:timeline-examples}
\end{table}

\subsubsection{Future directions}

There is significant work to be done to get us from knowing when access to the
Tor network is blocked, to knowing which pluggable transports work in which
censored countries, to using our censorship measurement data to inform
development of new pluggable transports. We don't currently have data about
whether bridges are blocked in many of the countries we've studied, let alone
which pluggable transports might be blocked. We need to develop more
sophisticated tests than OONI Probe's vanilla Tor and bridge reachability tests
to gather this data.

The type of testing we need to do may not fit into the OONI model\textemdash
OONI aims to deploy a simple, easy to use tool to its volunteers and some of
the Tor pluggable transports are relatively large and unwieldy. We might need
to explore other ways of deploying our tests and collecting data. For example,
we might release a modified version of Tor Browser that runs through the
included pluggable transports, testing to see if connections succeed, and then
uploading the results.

OONI might deploy new tests that not only establish a connection to the Tor
network, but also perform some activity, to ensure that the test traffic is
representative of that of a real user. A connection may be allowed to succeed
but then be disconnected shortly after, leaving Tor unusable from a user
perspective with no evidence of this being collected by OONI

Accurate and widespread measurement is essential for us to be able to respond
to censorship. The data that Tor Metrics and OONI collect gives us some
visibility into both ongoing censorship and discrete censorship events, but we
need to do much more.
