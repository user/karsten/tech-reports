Connections through the Tor network are usually slower than direct connections
between a user and a destination on the internet. A big part of this can be
explained by not enough resources being available on Tor relays, but other
factors are also at play~\cite{why-slow}, and can be particularly severe for
users with bad internet connections~\cite{pets2011-defenestrator}. In addition,
load on the network itself, including load created by an attacker or load
targeted at particular relays~\cite{torspinISC08}, can affect the performance
of the whole system. We need to better understand all of the factors that
contribute to poor performance for our users.

%Several factors contribute to making Tor connections less efficient than a
%direct connection to the final destination~\cite{why-slow}, and while a
%big part of that difference can be explained by the resources that are available
%at the Tor relays (see Section \ref{sec:network-health}), some of these
%performance factors are particularly severe on bad network
%connections~\cite{pets2011-defenestrator}. At the same time, we need to better
%understand how load on the network itself, including load created by an
%attacker or load targeted at particular relays~\cite{torspinISC08}, can impact
%all parts of the system.
%

\subsection{Performance in resource constrained access networks}

In the more developed world, recent advances in wired broadband networks and
cellular networks have led to faster speed when connecting to the internet. But
everyone has not seen these improvements. Connections over old DSL
infrastructure have hardly improved in over a decade, and some people can't
even get cellular data service. There is a large gap between the fastest and
the slowest service\textemdash a gap that continues to
deepen~\cite{HILBERT2016567}.

In the developed world, people usually had wired internet access first and
then got mobile (cellular) internet devices later. But some less developed
countries are skipping the construction of wired broadband networks altogether.
This is cost-effective because a single cell phone tower can provide service to
hundreds of customers.

According to the Egyptian Ministry for Communications and Information
Technology, there were 29.84 million internet users in Egypt in
2016~\cite{egypt-internet-users},
or about 37.8\% of the population. That number breaks down into 28.65 million
mobile internet subscribers and 4.6 million ADSL subscribers (the primary
wired broadband technology in Egypt), with some presumable overlap between
mobile and wired broadband users. The story is similar in other parts of the less developed world.

Mobile internet access has some unique challenges beyond the existing
challenges that users of wired internet face. These generally relate to the
variability of available bandwidth and delay.

Performance issues encountered in access networks are usually related to
bandwidth, latency (the delay between when a data packet is sent and
when it is received), data packet loss, and variations in the delays between
data packets (jitter). In some cases there may also be issues with out-of-order
delivery of data packets or intermittent connectivity. 

Tor uses the \emph{transmission control protocol} (TCP) for connections from
the Tor client to the guard relay, connections between the relays in the
circuit, and the connection from the exit to the destination. TCP provides both
\emph{congestion control} and \emph{flow control}. Congestion control
mechanisms manage the amount of traffic that enters the network to keep it from
being overloaded. Flow control mechanisms limit the amount of traffic sent to
the amount that the receiver is able to handle.

Ways to measure the maximum throughput---the rate of successful message
delivery---for a TCP connection over a given access network are well
understood~\cite{rfc6349}, however this is only one part of the Tor circuit.
There are multiple TCP connections in a Tor circuit, which makes measurement
more difficult. Initial work on measuring the performance of Tor from an
end-user perspective is detailed in Section \ref{sec:onionperf}.

When relays or bridges are under an attack intended to create a denial of
service, bandwidth is reduced, data packet loss rises, and latency and jitter
increase. Until connectivity is completely lost, the Tor protocol and the
pluggable transports used with Tor should ideally be able to degrade gracefully
by adapting to the changing network conditions.

\subsection{Improving client performance}

We plan to improve client performance both for users who have bad network
connections and also to ensure that performance degrades gracefully when the
network is under attack.

\subsubsection{End-to-end performance}

Network characteristics such as latency, bandwidth, data packet loss, and
jitter are highly interrelated. For example, in TCP connections such as those
used by Tor, increased latency can slow the time it takes until TCP allows the
available bandwidth to be fully used. Data packet loss and out-of-order delivery
of data also increase latency, as it increases the time before the software
that receives the data can put it back together correctly.

Tor has traditionally relied on end-to-end flow control to avoid overloading
clients and servers, but has not had a good solution for controlling
congestion at the Tor relays. There are many locations in a Tor relay where
data must be queued before it can be sent, and this can increase the latency in
the network. These locations include network hardware, operating system
interfaces and the Tor software. 

\paragraph{Congestion control}

%In looking to reduce the delays introduced by uncontrolled congestion in Tor,
%researchers have explored alternative classes of congestion and flow control.
%In one example~\cite{pets2011-defenestrator}, leveraging Tor's existing
%end-to-end window-based flow control framework and evaluating the performance
%benefits of using small fixed-size circuit windows, reducing the amount of data
%in flight that may contribute to congestion. In a second
%example~\cite{pets2011-defenestrator}, a dynamic window resizing algorithm that
%uses increases in end-to-end circuit round-trip time as an implicit signal of
%incipient congestion.

Researchers have explored alternative designs for congestion and flow control
in order to decrease the delays introduced by uncontrolled congestion in Tor.
One example~\cite{pets2011-defenestrator} proposes to restrict the amount of
data that is traveling through the network at any given time to a small, fixed
amount per client. Another idea~\cite{pets2011-defenestrator} is to vary that per-client amount based on observed network performance on the Tor circuit.

A third example~\cite{pets2011-defenestrator} uses a fresh approach to
congestion and flow control inspired by standard techniques from Asynchronous
Transfer Mode (ATM) networks. Here, Tor relays explicitly inform their
neighbors of how much traffic they can accept before becoming congested. This
reduces unnecessary delays and memory consumption.

Analysis has shown that the first two approaches offer up to 65\% faster web
page response times relative to Tor's current design. However, they are overly
conservative in the amount of traffic they allow into the network, resulting
in slower download times. In contrast, analysis of experiments with the third
proposal show that web clients experience up to 65\% faster web page responses
and a 32\% decrease in web page download times compared to Tor's current
design---though this proposal removes per-stream (end to end) flow
control, so it can't be deployed without further design work. These
results show that there
is clearly room for improvement, although further
research is needed before we decide what to implement.

\paragraph{Improved socket management}

Congestion does not only occur within Tor relay software or within the networks
that join them together; it can also occur in the operating system that the Tor
software runs on. When the Tor relay software decides to send data on a Tor
circuit, the operating system will sometimes queue the data before it sends it.
Some analysis~\cite{jansen14-kist} has indicated that congestion
occurs almost exclusively inside of the operating system's outgoing queues,
dwarfing delays caused by the Tor software itself.

This work went on to develop a new algorithm, KIST, to manage which circuit's
data to process at a given time. It exposes the operating system's queues to
the relay software, allowing Tor to choose circuits whose data will be sent
immediately, rather than being queued by the operating system. That is,
when the operating system tells the Tor software that many sockets are
writeable, but most of those sockets secretly already have a lot of
data queued inside the operating system, it is not possible for the Tor
software to make good queuing choices.

KIST reduced
circuit congestion by over 30\%, reduced network latency by 18\%, and increased
network throughput by nearly 10\% in experiments. This improved algorithm has
been included in the Tor relay software since version 0.3.2.


\subsubsection{Resumable uploads}

Some users may have access networks that provide intermittent connectivity.
This can occur even with good infrastructure when, for example, a user switches
between their cellular data connection and a local Wi-Fi connection. For web
browsing, where many small requests (for web pages, for example) are made, this
sort of dropped connectivity may be noticeable, but recovery happens relatively
quickly. However, if a user is half way through uploading a large file and the
connection drops, it can be very irritating. For users under duress, the
situation may be more serious. They may have a limited timeframe during which
they can upload a file and having to begin all over again may prevent them from
doing it.

JavaScript libraries, such as
Resumable.js~\cite{resumable-js}, which uses the HTML 5
File API~\cite{fileapi}, provide resumable upload functionality for web
applications to help address this problem. In the future, a helper function
could be added to Tor Browser to assist in resuming large uploads or downloads
in the event that connectivity is interrupted. If transfers resume
automatically this may even be transparent to the user.

\subsubsection{Striping connections}

Bridges providing access to the Tor network via pluggable transports often have
lower bandwidth available than regular relays. To help in load-balancing across
available bridges and avoid wasting capacity, and also to improve end-user
performance, user traffic can be split across multiple bridges.
Conflux~\cite{pets13-splitting} is one such approach to dynamic
traffic-splitting that assigns traffic to a Tor circuit based on its measured
latency.

Experiments have shown an improvement of approximately 30\% in expected
download time for web browsers that use multiple Tor bridges and for
streaming video applications.
% ^ what does this mean? XXX
This method introduces tradeoffs between users'
anonymity and performance. As with congestion control however, there is
clearly room for improvement. Further research is needed to decide on a path
forward.
