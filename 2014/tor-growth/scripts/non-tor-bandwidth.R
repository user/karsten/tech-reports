rm( list = ls() )
setwd('/Users/know/Desktop/tor analytics/non-tor-bw/')


library(plyr) # for renaming columns

#######################################################
D_netindex <- read.csv('country_daily_speeds.csv')[c('date','country_code','download_kbps')]

D_netindex$download_kbps <- D_netindex$download_kbps * (1000 / 1024)
D_netindex$upload_kbps <- D_netindex$upload_kbps * (1000 / 1024)



D_US = subset( D_netindex, country_code=='US' )
D_DE = subset( D_netindex, country_code=='DE' )
D_RU = subset( D_netindex, country_code=='RU' )

# merge the US, DE, and RU bandwidths
D_temp <- merge( D_US, D_DE, by='date' )
D_COMPOSITE <- merge( D_temp, D_RU, by='date' )

# drop the country codes
D_COMPOSITE <- D_COMPOSITE[ , -which(names(D_COMPOSITE) %in% c('country_code.x','country_code.y','country_code'))]

# average the download KiB/s entries into one
D_COMPOSITE$avr_download_KBps <- NA
D_COMPOSITE$avr_download_KBps <- (D_COMPOSITE$download_kbps.x + D_COMPOSITE$download_kbps.y + D_COMPOSITE$download_kbps) / 3.0

# drop the country-specific download rates
D_COMPOSITE <- D_COMPOSITE[ , -which(names(D_COMPOSITE) %in% c('download_kbps.x','download_kbps.y','download_kbps'))]



plot( 1:nrow(D_US), log2(D_US$download_kbps), yaxt='n', xaxt='n', col='red', cex=0.7, xlab="Year", ylab="Mean download bandwidth (KiB/s)", pch=20 )
points( 1:nrow(D_RU), log2(D_RU$download_kbps), yaxt='n', xaxt='n', col='blue', cex=0.7, pch=20 )
points( 1:nrow(D_DE), log2(D_DE$download_kbps), yaxt='n', xaxt='n', col='orange', cex=0.7, pch=20 )

points( 1:nrow(D_COMPOSITE), log2(D_COMPOSITE$avr_download_KBps), col='black', cex=0.5, pch=20 )

####### Set the pretty Y-axis ############################
par(las=1)
#lab <- seq(from=1,to=45,by=1)
lab <- c(12,12.5,13,13.5,14,14.5)
axis(2,at=lab,labels=parse(text=paste("2^", lab, sep="")) )
##########################################################


####### Set the pretty X-axis ###################################
YearLabels=c('2009','2010','2011','2012','2013','2014')
YearLocations=c(367, 732, 1097, 1462, 1828, 2193)
axis(1,at=YearLocations,labels=YearLabels )
#################################################################


legend_texts = c("United Staes", "Germany", "Russia", "Composite")
legend( "topleft", legend=legend_texts, inset=0.01, pch=c(20,20), col=c('red','orange','blue','black') ) 



day <- 1:nrow(D_US)

mm <- lm( log2(download_kbps) ~ day, data=D_US )
1.0 / (mm$coefficients["day"] * 30)

mm <- lm( log2(download_kbps) ~ day, data=D_DE )
1.0 / (mm$coefficients["day"] * 30)

mm <- lm( log2(download_kbps) ~ day, data=D_RU )
1.0 / (mm$coefficients["day"] * 30)
