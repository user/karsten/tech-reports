\section{Introduction}
Starting on August 20, 2013 the Tor network has seen a rapid spike in the
number of directly connecting users.  This spike is apparently due to
the large ``mevade'' click-fraud botnet running its command and
control (C\&C)  as a Tor Hidden Service.  Figure~\ref{fig:users} shows that estimated
daily clients increased from under 1 million to nearly 6 million in
three weeks. Figure~\ref{fig:torperf} shows the effects on
performance:  measured downloading times for a 50 KiB file doubled,
from 1.5 seconds to 3.0 seconds.

\begin{figure}[!h]
\vspace{-12pt}
\begin{center}
\includegraphics[width=0.6\textwidth]{graphs/user-count}
\end{center}
\vspace{-12pt}
\caption{Estimated daily Tor users, 18 August to 13 September 2013}\label{fig:users}
\vspace{-12pt}
\end{figure}

\begin{figure}
\begin{subfigure}[t]{0.5\textwidth}
\includegraphics[width=\textwidth]{graphs/download-time}
\vspace{-12pt}
\caption{Measured 50 KiB download times, in seconds}\label{fig:torperf}
\end{subfigure} %
\begin{subfigure}[t]{0.5\textwidth}
\includegraphics[width=\textwidth]{graphs/bandwidth}
\vfill
\caption{Reported Bandwidth Histories, all relays.}\label{fig:bandwidth}
\end{subfigure}
\vspace{-12pt}
\caption{Download times and total bytes carried by the Tor network.}
\end{figure}

However, the amount of traffic being carried by the network did not
change dramatically, as seen in Figure~\ref{fig:bandwidth}. The
primary cause of the problems seems to be the increased processing
load on Tor relays caused by the large increase in key exchanges required to build anonymous encrypted tunnels, or
{\em circuits}.
When a Tor client - an {\em Onion Proxy} or OP - connects to the
network, it sends a
{\sc create} cell to a Tor node, called a {\em guard}, which contains
the first message $g^a$ in a Diffie-Hellman key exchange, called an ``onion
skin''; the node receiving the create cell computes the shared key
$g^{ab}$ and replies with the second message $g^b$, creating a $1$-hop
circuit.  After this, the client iteratively sends onion skins in {\sc extend} cells to the end of the circuit,
which extracts the onion skins and sends them in {\sc create} cells to
the next relay, until all three hops have exchanged keys.
\begin{itemize}
\item Extending a circuit -- decrypting an ``onion skin'' and
  participating in a Diffie-Hellman key exchange -- is sufficiently
  compute expensive that high-weight relays can become CPU-bound.  The
  total bandwidth that high-weight relays can handle in onion-skins is
  significantly lower than the network bandwidth.

\item The hidden service protocol -- explained in
  section~\ref{sec:background} -- causes at least three circuits to be
  built every time a bot connects.

\item When onion skins exceed the processing capacity of an OR, they
  wait in decryption queues, causing circuit building latencies to
  increase.

\item Queued onion skins eventually time out either at the relay or
  the client, causing the entire partial circuit to fail, causing more
  onion skins to be injected to the network.
\end{itemize}
\begin{figure}[t]
\vspace{-12pt}
\begin{center}
\includegraphics[width=0.6\textwidth]{graphs/extend-fail}
\end{center}
\vspace{-12pt}
\caption{Hourly measured failure rates, starting 27 September 2013, of {\sc extend} cells, for latest stable
  Tor release (0.2.3.25) and {\sf ntor}-prioritizing release candidate
  (0.2.4.17-rc).}
\label{fig:fail}
\vspace{-12pt}
\end{figure}
In response to this, the Tor Project modified release candidate
0.2.4.17-rc to prioritize processing of onionskins using the more
efficient {\sf ntor}~\cite{ntor} key exchange protocol.  Adoption of this release
helped the situation: as Figure~\ref{fig:torperf} shows,
measured download 50 KiB times as of late September decreased to
roughly 2.0 seconds.  Figure~\ref{fig:fail} shows that circuit
extensions using tor version 0.2.4.17-rc range between 5\% and 15\%, while circuit
extensions using the stable release, version 0.2.3.25, ranged between
5\% and 30\%.  By November 2013, further efforts to find and remove the infection by
anti-malware teams from companies including Microsoft have mitigated
the immediate threat, though the several unmanaged hosts remaining
could still revive the botnet.

In this document, we consider longer-term strategies to ease the load
on the network and reduce the impact on clients.   Full evaluation of
the effectiveness of these strategies, impact on privacy and
performance for regular users, and relative ease of deployment remains
an ongoing challenge; the Tor Project welcomes the collaboration of the
research and anonymity community in meeting this challenge.

%[ This is where the threat model(s) go: botmaster running all C\&C
%through Tor, adaptive in the long term, perhaps not in the medium term ]

We assess these strategies with the  security goal of ensuring the
availability of Tor under the threat of a botnet that uses hidden services
as its primary C\&C channel.  A ``medium-term'' strategy is one which
might be successful against a botnet that does not modify the behavior
of basic Tor binaries and does not respond strategically to changes in
Tor designed to mitigate botnet-induced load.   On the other hand, a
``long-term'' strategy must contend with a botnet that could be
deployed in response to such methods, where the behavior of both the
botnet and the tor software can change adaptively to circumvent
mitigation mechanisms.   We note that some attacks are out of the
scope of this document, in particular we do not consider a botnet that
simply communicates with non-hidden servers through Tor, since such an
attack must contain traditional network addresses, and we do not
consider a botnet that simply seeks to conduct a denial of service
attack on Tor by flooding the network with traffic in excess of its capacity.

The remainder of this manuscript describes the major categories of
medium- and long-term responses that Tor could consider, along with
the technical challenges that each would pose to the research
community.  Section~\ref{sec:bot} considers medium-term strategies
aimed at eliminating a current botnet threat to Tor.
Section~\ref{sec:throttle}  considers longer-term mechanisms to
limit the rate of circuit-building requests by any botnet.
Section~\ref{sec:fail} describes mechanisms to reduce the load from
ordinary clients.  Section~\ref{sec:iso} considers the idea
of isolating hidden-service from regular Tor client traffic.

\section{Background: Tor Hidden Services} \label{sec:background}

The Tor network provides a mechanism for clients to anonymously
provide services (e.g., websites) that can be accessed by other users
through Tor.  We briefly review the protocol for this mechanism:
\begin{enumerate}
\item The hidden service (HS) picks a public ``{\em identity key}'' $PK_S$ and
  associated secret key $SK_S$.   The HS then computes an ``{\em onion
  identifier}'' $o_S = H(PK_S)$ using a cryptographic hash function
  $H$.   Currently, the hash function $H$ is the output of SHA1,
  truncated to 80 bits.  This 10-byte identifier is base32-encoded to
  produce a 16-byte \url{.onion} address that Tor users can use to
  connect to HS, such as \url{3g2upl4pq6kufc4m.onion}.
\item The HS constructs circuits terminating in at least
  three different relays, and requests these relays to act as its {\em
    introduction points}  (IPs).
\item The HS then produces a ``{\em descriptor},'' signed using
  the $SK_S$, that lists $PK_S$ and its IPs.  This
  descriptor is published through a distributed hash ring of Tor
  relays, using $o_S$ and a time period $\tau$ as an index. 
\item A client OP connects to the HS by retrieving
  the descriptor using $o_S$ and $\tau$, and
  building two circuits: one circuit terminates at an IP and
  the other terminates at a randomly-selected relay
  referred to as the {\em rendezvous point} (RP).  The client asks the
  IP to send the identity of the RP to the HS.
\item The HS then builds a circuit to the RP, which
  connects the client and HS.
\end{enumerate}
Since lookups to the distributed hash ring are performed through
circuits as well, and each descriptor has three redundant copies,  a
client connecting to a hidden service could require building up to 6
circuits; to reduce this load, clients cache descriptors and reuse rendezvous
circuits any time a request is made less than ten minutes after the
previous connection.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "botnet-tr"
%%% End: 
