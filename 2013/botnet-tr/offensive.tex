\section{Attack the Botnet} \label{sec:bot}

One set of possible medium-term responses would be to build defenses
that protect Tor against abuse by the current botnet.  We consider an
escalating series of possible mechanisms, depending on the adaptivity of
the botnet.

\subsection{Descriptor Blacklisting}
Discover the \url{.onion} address of the C\&C hidden service using
some variant of Biryokov {\em et al}'s ``Trawling for Hidden
Services'' attack~\cite{oakland2013-trawling}.  Essentially, the
descriptor can be recognized by its popularity - the volume of
requests is an order of magnitude larger than all other hidden
services combined.  Once the descriptor has been discovered,
blacklisting the HS public key from the Hidden Service directory will
prevent attempts to reach the blacklisted \url{.onion} address by
clients.  

Unfortunately, this approach is both technically and philosphically
problematic.   Existing Tor client software does not deal gracefully
with failed descriptor lookups, leading to a situation where lookup
failures increase the circuit load on the network.  Furthermore,
an adaptive or forward-looking botmaster can defeat
identification through volume by multiplexing across multiple
\url{.onion} addresses. Blacklisting can be defeated using multiple
keys or domain generation algorithms -- in the hidden service case,
generating a sequence of public keys using a fixed-seed pseudorandom
sequence.  Philosophically, a mechanism to blacklist certain hidden
service keys could potentially be abused for censorship. 

\subsection{Deanonymize the server}
Assuming the botnet is prepared for blacklisting, the Tor Project
could instead attempt to discover the entry guards of the C\&C server,
repeatedly changing their availability (e.g. by rotating identity
keys), until a colluding entry guard is chosen, eventually learning
the IP address of the C\&C server.  Once the server is deanonymized,
traditional anti-malware organizations could be alerted, hopefully
taking the botnet offline.  From a techincal standpoint, coordinating
an attack of this scale, including the repeated coercion or
identity-rotation of entry guards, while not further disrupting the
network, would seem to pose significant organizational and engineering
challenges.  Depending on the fraction of colluding entry-guard
bandwidth, the time-scale of the attack could also be problematic.



\subsection{Fingerprint and blacklist clients}
A related, less costly, alternative is to look for connections to this
hidden service (e.g. by waiting until colluding nodes are chosen as
Introduction Points or Rendezvous points) and attempt to
``fingerprint'' a bot's ``phone home'' connection
~\cite{wpes13-fingerprinting,ccs2012-fingerprinting,ccs2011-stealthy,wpes11-panchenko}.
This fingerprint could then be used to temporarily blacklist bots
and/or the C\&C server at entry guards.  Deploying the solution would
involve technical challenges such as determining a stable fingerprint,
and upgrading a significant fraction of relays to support fingerprint
matching and blacklisting.  Furthermore, an adaptive botnet could make
this very difficult by employing many of the same traffic analysis
countermeasures employed by obfuscated transport mechanisms, for
example,  ``morphing''~\cite{morphing09} C\&C
connections to look like interactions with various popular
destinations (e.g. choosing at random an Alexa Top 1000 web site, or
even running a relay and choosing at random another stream to
emulate).  

\subsection{General Objections}
Beyond the technical difficulties and the possibility for abuse of any
of the mechanisms described in this section, another concern is that
``attacking'' a botnet with hundreds of thousands or even millions of
nodes could lead to retaliation.  So far the network has withstood the
onslaught of new clients created by this bot, but a concerted attack
by a botnet of this size could easily bring down the entire Tor
network.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "botnet-tr"
%%% End: 
