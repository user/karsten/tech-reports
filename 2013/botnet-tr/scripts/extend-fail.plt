set yrange [0:]
set xrange [0:]
set ylabel "onionskin fail rate"
set xlabel "hour"
set xtics 12
set size 1.0,0.5 
set grid lw 4
set datafile missing '-'
set terminal postscript portrait enhanced color dashed lw 1 "Helvetica" 18
set output "../graphs/extend-fail.eps"
plot "../data/ef-rc/rc.dat" using ($3-6475):($6):7:8 title "0.2.4.17-rc" with yerrorlines linecolor rgbcolor "medium-blue" lw 4, \
     "../data/ef-stable/stable.dat" using ($3-6475):($6):7:8 title "0.2.3.25" with yerrorlines lc rgbcolor "dark-red" lw 4
